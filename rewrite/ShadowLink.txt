#!name=ShadowLink VPN
#!desc=版本 4.3.4

[Script]
ShadowLink VPN = type=http-response,pattern=https://buy.itunes.apple.com/verifyReceipt,script-path=https://gitlab.com/ppxiao/quantumultx/-/raw/main/Script/shadowlink.js,requires-body=1,max-size=0

[MITM]
hostname = %APPEND% buy.itunes.apple.com
